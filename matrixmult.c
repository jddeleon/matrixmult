/* File:     matrixmult.c
 * Author:   Jesse De Leon
 * Purpose:  Use MPI to implement a parallel version of multiplying 
 *           two n x n matrices. 
 *
 * Input:    form - "ijk, ikj, or kij
 *           flag - 'R' or 'I' - R to generate random matrix, I to read from stdin
 *           n - number of processes
 *           A - If I was entered for flag, enter Matrix A
 *           B - If I was entered for flag, enter matrix B
 *
 * Output:   matrix A multiplied with matrix B
 *
 * Compile:  mpicc -g -Wall -o matrixmult matrixmult.c
 * Run:      mpiexec -n <number of processes> ./matrixmult */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <mpi.h>

#define F_MIN 0.0
#define F_MAX 9.0

//#define DEBUG

typedef enum {IJK, IKJ, KIJ} Form;

void Enter_the_matrix(int my_rank, int comm_sz, double** a,
                         double** b, double** c, double** tmp_a, double** local_c, int* n, 
                         int* local_n, Form* form_type, MPI_Comm comm);
double  fRand(double min, double max);
void    Generate_random(double** matrix, int n);
void    Read_matrix(double** matrix, int n);
int     Malloc_matrix(double*** matrix, int n, int m);
int     Free_matrix(double*** matrix); 
void    Print(char title[], double** matrix, int n);
void    Check_for_error(int local_ok, char fname[], char message[], 
                        MPI_Comm comm);
void    Matrix_mult(double** a, double** b, double** local_c, int n, int local_n, MPI_Comm comm);

int main(void) {
    int my_rank;
    int comm_sz;
    int n;
    int local_n;

    Form form_type;
           
    double** a = NULL;
    double** b = NULL;
    double** c = NULL;
    double** tmp_a = NULL;
    double** local_c = NULL;

    double local_start;
    double local_finish;
    double local_elapsed;

    MPI_Init(NULL, NULL);
    MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);
    MPI_Comm_size(MPI_COMM_WORLD, &comm_sz);

    Enter_the_matrix(my_rank, comm_sz, a, b, c, tmp_a, local_c, &n, &local_n, &form_type, 
                     MPI_COMM_WORLD);

    if(my_rank == 0)
    {
        // begin timing
        local_start = MPI_Wtime();
        
        /* Broadcast n to all processes */
        MPI_Bcast(&n, 1, MPI_INT, 0, MPI_COMM_WORLD);

    }

    /* every process now allocates matrix A, which contains local_rows only
       and also a local_c, which contains the local result*/
    if (Malloc_matrix(&a, local_n, n) == -1)
    {
        printf("Error allocating matrix A...\n");
        exit(1);
    }
    if (Malloc_matrix(&local_c, local_n, n) == -1)
    {
        printf("Error allocating matrix A...\n");
        exit(1);
    }

    /* scatter tmp_a to all processes */
    if (my_rank == 0)
    {
       // MPI_Scatter(&(tmp_a[0][0]), local_n*n, MPI_DOUBLE, 
         //       &(a[0][0]), local_n*n, MPI_DOUBLE, 0, MPI_COMM_WORLD);
        MPI_Bcast(&(b[0][0]), n*n, MPI_DOUBLE, 0, MPI_COMM_WORLD);
        Free_matrix(&tmp_a);
    }
    
    MPI_Barrier(MPI_COMM_WORLD);
    Matrix_mult(a, b, c, n, local_n, MPI_COMM_WORLD);

    MPI_Gather(&(local_c[0][0]), local_n * n, MPI_DOUBLE,
               &(c[0][0]), local_n * n, MPI_DOUBLE, 0, MPI_COMM_WORLD);
               
    if (my_rank == 0)
    {
        // end of timing
        local_finish = MPI_Wtime();
        local_elapsed = local_finish - local_start;
        printf("elapsed time = %lf seconds\n", local_elapsed);
    }

    // Goodbye, COMM_WORLD
    MPI_Finalize();

    return 0;
} /* end of main */

/*-------------------------------------------------------------------
 * Function:  Matrix_mult
 * Purpose:   Multiply a matrix A by a vector x.  The matrix is distributed
 *            by block rows and the vectors are distributed by blocks
 * In args:   a:  calling process' rows of matrix A
 *            b:  calling process' entire matrix B
 *            local_c:  calling process' local result matrix 
 *            n:        global (and local) number of columns
 *            local_n:  calling process' number of rows
 *            comm:     communicator containing all calling processes
 * Errors:    if malloc of local storage on any process fails, all
 *            processes quit.            
 * Notes:
 * 1.  comm should be MPI_COMM_WORLD because of call to Check_for_errors
 * 2.  local_n should be the same on all the processes
 */
void Matrix_mult(double** a, double** b, double** local_c, int n, int local_n, MPI_Comm comm)
{
    double sum = 0.0;

    /* ijk form */
    for (int i = 0; i < local_n; i++)
    {
        for (int j = 0; j < n; j++)
        {
            for (int k = 0; k < n; k++)
            {
                sum = sum + a[i][k] * b[k][j]; 
            }

            local_c[i][j] = sum;
            sum = 0.0;
        }
    }
} /* end of Matrix_mult */

/*-------------------------------------------------------------------
 * Function:  Check_for_error
 * Purpose:   Check whether any process has found an error.  If so,
 *            print message and terminate all processes.  Otherwise,
 *            continue execution.
 * In args:   local_ok:  1 if calling process has found an error, 0
 *               otherwise
 *            fname:     name of function calling Check_for_error
 *            message:   message to print if there's an error
 *            comm:      communicator containing processes calling
 *                       Check_for_error:  should be MPI_COMM_WORLD.
 *
 * Note:
 *    The communicator containing the processes calling Check_for_error
 *    should be MPI_COMM_WORLD.
 */
void Check_for_error(
      int       local_ok   /* in */, 
      char      fname[]    /* in */,
      char      message[]  /* in */, 
      MPI_Comm  comm       /* in */) {
   int ok;

   MPI_Allreduce(&local_ok, &ok, 1, MPI_INT, MPI_MIN, comm);
   if (ok == 0) {
      int my_rank;
      MPI_Comm_rank(comm, &my_rank);
      if (my_rank == 0) {
         fprintf(stderr, "Proc %d > In %s, %s\n", my_rank, fname, 
               message);
         fflush(stderr);
      }
      MPI_Finalize();
      exit(-1);
   }
}  /* Check_for_error */

/*-------------------------------------------------------------------
 * Function:  Generate_random
 * Purpose:   Produce an n x n matrix of random doubles
 *
 * In args:   matrix:   a previously allocated matrix
 *            n:        order of matrix
 *
 * Output:    matrix is populated with random values 
 */
void Generate_random(double** matrix, int n)
{
    for (int row = 0; row < n; row++)
        for (int col = 0; col < n; col++)
            matrix[row][col] = fRand(F_MIN, F_MAX);
} /* end of Generate_random */

/*-------------------------------------------------------------------
 * Function:  Enter_the_matrix
 * Purpose:   Get input from stdin to set up the necessary matrices
 *            
 * In args:   my_rank:  rank of calling process
 *            comm_sz:  total number of processes
 *            comm      MPI_COMM_WORLD
 * Out args
 *            a:        Matrix A
 *            b:        Matrix B
 *            c:        Matrix C
 *            tmp_a:    Temporay global matrix before being scattered
 *            local_c:  stores local results
 *            n:        order of matrices/ number of columns
 *            local_n:  number of local rows for each process
 *            form_type:ijk, ikj, kij
 *              
 * 1.  comm should be MPI_COMM_WORLD because of call to Check_for_errors
 */
void Enter_the_matrix(int my_rank, int comm_sz, double** a, double** b,
                 double** c, double** tmp_a, double** local_c, int* n, int* local_n, Form* form_type, MPI_Comm comm)
{

    char type[4];
    char flag[2];
 

    if (my_rank == 0)
    {
        
        scanf("%s", type);
        scanf("%s", flag);
        scanf("%i", n); 
       
        if (*n <= 0 || *n % comm_sz != 0)
        {
            Check_for_error(0, "Enter_the_matrix", 
                "n must be positive and evenly divisible by comm_sz", comm);
        }
        
        /* allocate tmp_a, B, and C */
        if(Malloc_matrix(&tmp_a, *n, *n) == -1)
        {
            printf("Error allocating matrix A...\n");
            exit(1);
        }
        if(Malloc_matrix(&b, *n, *n) == -1)
        {
            printf("Error allocating matrix B...\n");
            exit(1);
        }
        if(Malloc_matrix(&c, *n, *n) == -1)
        {
            printf("Error allocating matrix C...\n");
            exit(1);
        }
            
        #ifdef DEBUG
        printf("Line 201\n");
        #endif
        if (strcmp(type, "ijk"))
            *form_type = IJK;
        else if (strcmp(type, "ikj"))
            *form_type = IKJ;
        else if (strcmp(type, "kij"))
            *form_type = KIJ;
        else
        {
            printf("Invalid form. Giving up...\n");
            exit(1);
        }

        if (flag[0] == 'R')
        {
            // generate random matrices for A and B
            Generate_random(tmp_a, *n);
            Generate_random(b, *n);

        #ifdef DEBUG
        printf("Line 222\n");
        #endif
        }
        else if (flag[0] == 'I')
        {
            // read matrices from stdin
            Read_matrix(tmp_a, *n);
            Read_matrix(b, *n);
        #ifdef DEBUG
        printf("Line 231\n");
        #endif
        }
        else // error
        {
            printf("Invalid flag. Giving up...\n");
            exit(1);
        }
        #ifdef DEBUG
        printf("Line 233\n");
        #endif
        #ifdef DEBUG
        Print("A", tmp_a, *n);
        Print("B", b, *n);
        #endif

        #ifdef DEBUG
        printf("Line 241\n");
        #endif
        
        *local_n = *n/comm_sz;
    }

        #ifdef DEBUG
        printf("Line 267\n");
        #endif
}

/*----------------------------------------------------------------------------- 
 * Function:    fRand
 * Purpose:     Return a pseudo-random double value
 *
 * Input Args:  min: minimum value that can be generated
 *              max: maximum value that can be generated
 *
 * Output:      randomly generated double value 
 */
double fRand(double min, double max)
{
    double f = (double)rand() / RAND_MAX;
    return min + f * (max  - min);
} // end of fRand

/*-------------------------------------------------------------------
 * Function:  Read_matrix
 * Purpose:   Read in a matrix from stdin
 *
 * In args:   matrix:   allocated matrix to store values
 *            n:        global number of components
 *
 * Out args:  matrix:   matrix containing values read from standard input 
 */
void Read_matrix(double** matrix, int n)
{
    for (int row = 0; row < n; row++)
    {
        for ( int col = 0; col < n; col++)
        {
            scanf("%lf", &matrix[row][col]);
        }
    }
} /* end of Read_matrix */

/*-------------------------------------------------------------------
 * Function:  Malloc_matrix 
 * Purpose:   Allocate a two dimensional dynamic array in contiguous memory
 *
 * In args:   n:          number of rows
 *            m:          number of columns
 *
 * Out args:  matrix      allocated matrix
 * Note: This code was found on stackoverflow
 */
int Malloc_matrix(double*** matrix, int n, int m)
{
    // allocate the n*m contiguous items
    double *ptr = (double*)malloc(sizeof(double) * n*m);
    if (!ptr)
        return -1;

    // allocate the row pointers into memory
    (*matrix) = (double**)malloc(sizeof(double*) * n);
    if (!(*matrix))
    {
        free(ptr);
        return -1;
    }

    // set up the pointers into contiguous memory
    for (int i = 0; i < n; i++)
        (*matrix)[i] = &(ptr[i*m]);

    return 0;  
} /* end of Malloc_matrix */

/*-------------------------------------------------------------------
 * Function:  Free_matrix 
 * Purpose:   free allocated matrix from memory
 *
 * In args:   matrix:     matrix to be freed
 * NOTE:    This code was found on stackoverflow
 */

int Free_matrix(double*** matrix) 
{
    /* free the memory - the first element of the array is at the start */
    free(&((*matrix)[0][0]));

    /* free the pointers into the memory */
    free(*matrix);

    return 0;
} /* end of Free_matrix */

/*-------------------------------------------------------------------
 * Function:  Print
 * Purpose:   Print a matrix
 * In args:   title:      name of matrix
 *            matrix      matrix to be printed
 *            n:          global number of components
 */
void Print(char title[], double** matrix, int n)
{
    printf("\nMatrix %s\n", title);

    for (int row = 0; row < n; row++)
    {
        for (int col = 0; col < n; col++)
        {
            printf("%lf", matrix[row][col]);
            if (col != n-1)
                printf(" ");
        }

        printf("\n");
    }

    printf("\n");
} /* end of Print */
