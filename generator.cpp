/* File:        generator.cpp
 * Author:      Jesse De Leon
 *
 * Purpose:     generates two n x n matrices of doubles to use as input
 *              for matrixmult.c
 *
 * Usage:       ./generator <form> <n> <min> <max>
 *
 * Input:       <form> is "ijk", "ikj", or "kij" - this is simply echoed to output
 *              <n> specifies the size of each matrix in n x n form
 *              <min> minimum value to generate
 *              <max> maximum value to generate
 *
 * Output:      generated matrices
 */

#include <iostream>
#include <cstdlib>
#include <string>
using namespace std;

double fRand(double, double);

int main(int argc, char *argv[])
{
    string      form = argv[1];
    int         n = atoi(argv[2]);
    double      min = atoi(argv[3]);
    double      max = atoi(argv[4]);
    
    int count = 0;
    
    cout << form << endl << n << endl;

    while (count < 2)
    {
        // Output matrices
        for (int row = 0; row < n; row++)
        {
            for (int col = 0; col < n; col++)
            {
                
                cout << fRand(min, max);
                if (col != n-1)
                    cout << " ";
            }

            cout << endl;
        }

        count++;
    }
}

/*----------------------------------------------------------------------------- 
 * Function:    fRand
 * Purpose:     Return a pseudo-random double value
 *
 * Input Args:  min: minimum value that can be generated
 *              max: maximum value that can be generated
 *
 * Output:      randomly generated double value 
 */
double fRand(double min, double max)
{
    double f = (double)rand() / RAND_MAX;
    return min + f * (max  - min);
} // end of fRand
